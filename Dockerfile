#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE=alpine
ARG BASE_IMAGE_TAG=3.7
ARG BASE_IMAGE_NAMESPACE=

FROM ${BASE_IMAGE_NAMESPACE}${BASE_IMAGE}:${BASE_IMAGE_TAG}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY ./build/pre-build /usr/bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

RUN apk add --no-cache docker

ENV CREDENTIALS=/home/pi/.nextcloudsync
ENV SYNC=/config
ENV VERSION=latest

CMD /bin/sh -c "docker run -d \
  --rm \
  --name=nextcloudsync \
  -v $CREDENTIALS:/credentials \
  -v $SYNC:/localsync \
  --privileged \
  homesmarthome/nextcloudsync:$VERSION"

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY ./build/post-build /usr/bin/cross-build-end
RUN [ "cross-build-end" ]

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/nextcloudsyn_shuttelc" \
      de.5square.homesmarthome.description="Synching config files bi-directional from all homesmarthome instances to cloud" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
# nextcloudsync_shuttle

Shuttle Service for starting nextcloudsync container in privileged mode from Docker Swarm.

## Install as a Service with Portainer
1. Open portainer ([homesmarthome.local:9000](http://homesmarthome.local:9000)
2. Click on *Services* in the menu
3. Click on *+ Add Service*
4. Configuration:

   * Name: nextcloudsync_shutter
   * Image configuration
     * Name: `homesmarthome/nextcloudsync_shuttle:1.0.1`
   * Scheduling
     * Scheduling mode: `global`
   * Volumes (map additinal volume)
     * container: `/var/run/docker.sock` (`Bind`)
     * host: `/var/run/docker.sock`
   * Optional Command & Logging (map environment variable)
     * name: `CREDENTIALS`
     * value: `/home/pi/.nextcloudsync`
   * Optional Command & Logging (map environment variable)
     * name: `SYNC`
     * value: `/config`
   * Optional Command & Logging (map environment variable)
     * name: `VERSION`
     * value: `1.0.0`
   * Update config & Restart 
     * Restart delay: `600`

5. Actions
    * `Create the service`
docker_run:
	mkdir -p ./build/testrun_config
	docker run -d \
	  --rm \
	  --name=nextcloudsync_shuttle_test_run \
	  -v $(PWD)/test/.nextcloudsync:/credentials \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-e CREDENTIALS=$(PWD)/test/.nextcloudsync \
		-e SYNC=$(PWD)/test \
		-e VERSION=latest \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep nextcloudsync_shuttle_test_run

docker_stop:
	docker rm -f nextcloudsync_shuttle_test_run
	rm -rf ./testrun_config